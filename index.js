
const cache = require('memory-cache');
const moment = require('moment');
const debug = require('debug')('memcache');
let memCache = new cache.Cache();

function cacheMiddleware(duration) {
  return (req, res, next) => {
      const _duration = duration || (moment().endOf('day') - moment())/1000;
      let key =  '__express__' + req.originalUrl || req.url
      let cacheContent = memCache.get(key);
      if(cacheContent){
          debug(`Serving from Cache ${key}`);
          res.setHeader('Content-Type', 'application/json');
          res.send( cacheContent );
          return
      }else{
          debug(`Saving to Cache ${key}`);
          res.sendResponse = res.send
          res.send = (body) => {
              memCache.put(key,body,_duration*1000);
              res.sendResponse(body)
          }
          next()
      }
  }
}

module.exports = cacheMiddleware
